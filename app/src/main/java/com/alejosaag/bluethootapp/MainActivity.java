package com.alejosaag.bluethootapp;

import android.Manifest;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Set;


public class MainActivity extends AppCompatActivity {
    //Instancia de la clase bluetoothAdapter
    private BluetoothAdapter bluetoothAdapter;

    //Variables de los componentes graficos (TextView, Button, etc..)
    private TextView mStatusTv;
    private Button mScanBtn;
    private Button btnEnviarArchivo;

    //Instancia de la clase ProgressDialog
    private ProgressDialog mProgressDlg;

    //ArrayList para almacenar los dispositivos encontrados y apareados
    private ArrayList<BluetoothDevice> mDeviceList = new ArrayList<BluetoothDevice>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        //Referenciación de los componentes graficos en el Layout
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        btnEnviarArchivo = (Button) this.findViewById(R.id.btnEnviarArchivo);
        btnEnviarArchivo.setOnClickListener(btnEnviarArchivoClick);

        //Configuración del ProgressDialog
        mProgressDlg = new ProgressDialog(this);
        mProgressDlg.setMessage("Escaneando...");
        mProgressDlg.setCancelable(false);

        //Asignación del evento click al botón del ProgressDialog
        mProgressDlg.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                bluetoothAdapter.cancelDiscovery();
            }
        });


        //Configuración de filtros para la clase BluetoothAdapter
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);

        //Registro del CallBack (mReceiver) para gestionar las respuestas de los filtros configurados
        registerReceiver(mReceiver, filter);

    }


    //Metodo para encender el BlueTooth
    public void btnEncender(View v) {

        //Verificacón de que el Bluetooth no este encendido
        if (!bluetoothAdapter.isEnabled()) {
            Intent turnOn =
                    new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);

            startActivityForResult(turnOn, 0);
            Toast.makeText(getApplicationContext(),
                    "Encendido", Toast.LENGTH_LONG).show();

        } else { //En caso tal ya este encendido
            Toast.makeText(getApplicationContext(),
                    "Ya esta encendido", Toast.LENGTH_LONG).show();
        }
    }

    //Metodo para apagar el BlueTooth
    public void btnApagar(View v) {
        bluetoothAdapter.disable(); //Deshabilitar el bluetooth

        Toast.makeText(getApplicationContext(),
                "Apagado", Toast.LENGTH_LONG).show();
    }

    //Hacer visible (descubrile) el Bluetooth
    public void btnVisible(View v) {
        Intent getVisible = new
                Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);


        // getVisible.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);


        startActivityForResult(getVisible, 0);
    }

    //Evento para obtener el listado de dispositivos vinculados (apareados)
    public void btnLstApareados(View v) {

        //Obtención de l listado de dispositivos vinculados (apareados)
        Set<BluetoothDevice> dispositivosVinculados =
                bluetoothAdapter.getBondedDevices();

        //Si la lista es null, se imprimer el mensaje No se encontraron dispositivos vinculados
        if (dispositivosVinculados == null ||
                dispositivosVinculados.size() == 0) {
            showToast("No se encontraron dispositivos vinculados");
        } else { //En caso contrario se crea una lista (list) de dispositivos BluetoothDevice
            ArrayList<BluetoothDevice> list =
                    new ArrayList<BluetoothDevice>();

            list.addAll(dispositivosVinculados);

            //Envio del listado de dispositivos apareados a la Activity Listado
            Intent intent = new Intent(getBaseContext(),
                    Listado.class);
            intent.putParcelableArrayListExtra("device.list", list);
            startActivity(intent);
        }
    }

    //Opcional si se definieron explicitamente en el manifiesto
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void checkBTPermissions() { //Metodo de ejemplo (opcional) para comprobar permisos desde el código
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            int permissionCheck = this.checkSelfPermission("Manifest.permission.ACCESS_FINE_LOCATION");
            permissionCheck += this.checkSelfPermission("Manifest.permission.ACCESS_COARSE_LOCATION");
            if (permissionCheck != 0) {

                this.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION}, 1001); //Any number
            }
        } else {
            showToast("checkBTPermissions: No need to check permissions. SDK version < LOLLIPOP.");
        }
    }

    //Evento para comenzar el escaneo de dispositivos
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void btnEscanear(View v) {
        this.checkBTPermissions(); //Metodo para comprobar los permisos
        bluetoothAdapter.startDiscovery();
    }

    //Evento para enviar archivos a otro dispositivo
    private View.OnClickListener btnEnviarArchivoClick = new View.OnClickListener() {
        public void onClick(View v) {
            //Intent especial para explorar archivos en el celular
            Intent chooser = new Intent(Intent.ACTION_GET_CONTENT); //Multiples archivos: Intent.EXTRA_ALLOW_MULTIPLE
            Uri uri = Uri.parse(Environment.getDownloadCacheDirectory().getPath().toString());
            chooser.addCategory(Intent.CATEGORY_OPENABLE);
            chooser.setDataAndType(uri, "*/*");

            try {
                startActivityForResult(chooser, 1);
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(getBaseContext(), "Por favor instala un administrador de archivos.", Toast.LENGTH_SHORT).show();
            }
        }
    };


    //CallBack para gestionar las acciones relacionadas con el descubrimiento de dispositivos
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            //Condición para determinar si el estado del BlueTooth cambia (ejemplo: encendido o apagado)
            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                final int state = intent.getIntExtra(
                        BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.ERROR);

                if (state == BluetoothAdapter.STATE_ON) {
                    showToast("Habilitado");
                }
                //Condición para determinar "si" se están escaneando dispositivos
            } else if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
                //ArrayList mDeviceList para almacenar los dispositivos encontrados
                mDeviceList = new ArrayList<BluetoothDevice>();
                mProgressDlg.show(); //Iniciar el progressDialog
                //Condición para determinar "si" el escaneo de dispositivos termino
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                mProgressDlg.dismiss(); //Finalizar el progressDialog
                //Invocación de la Activity Listado, una vez termine el esncaneo
                //para mostrar los dispositivos encontrados
                Intent newIntent = new Intent(MainActivity.this,
                        Listado.class);
                //Envio del listado de dispositivos encontrados a la Activity listado
                newIntent.putParcelableArrayListExtra("device.list",
                        mDeviceList);

                startActivity(newIntent);
                //Condición para determinar "si" encontro algún dispositivo
            } else if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice dispositivo = (BluetoothDevice)
                        intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                mDeviceList.add(dispositivo);
                showToast("Dispositivo encontrado: " + dispositivo.getName());
            }

        }
    };

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (null == data) return;
            String selectedImagePath;
            Uri selectedImageUri = data.getData();

            String path = data.getData().getPath();

            //MEDIA GALLERY
            selectedImagePath = LibraryFilePath.getPath(getBaseContext(), selectedImageUri);
            //   showToast(selectedImagePath);

            Intent intent = new Intent();
            //ACTION_SEND_MULTIPLE
            intent.setAction(Intent.ACTION_SEND);

            intent.setType("*/*");
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(selectedImagePath)));
            startActivity(intent);


            //Log.i("Image File Path", ""+selectedImagePath);
            //ShowMessageDialog(""+selectedImagePath);

        }
    }

    //Metodo para imprimir mensajes tipo Toast
    private void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(mReceiver);
        super.onDestroy();
    }
}