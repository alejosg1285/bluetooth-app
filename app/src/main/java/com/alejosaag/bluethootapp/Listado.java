package com.alejosaag.bluethootapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.widget.ListView;
import android.widget.Toast;

import java.lang.reflect.Method;
import java.util.ArrayList;

public class Listado extends AppCompatActivity {

    private ListView mListView;
    private DispositivosListAdapter mAdapter;
    private ArrayList<BluetoothDevice> mDeviceList;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado);
        mDeviceList = getIntent().getExtras()
                .getParcelableArrayList("device.list");

        mListView = (ListView) findViewById(R.id.lv_paired);
        mAdapter = new DispositivosListAdapter(this);
        mAdapter.setData(mDeviceList);
        mListView.setAdapter(mAdapter);


        mAdapter.setListener(new DispositivosListAdapter
                .OnPairButtonClickListener() {
            @Override
            public void onPairButtonClick(int position) {
                BluetoothDevice device = mDeviceList.get(position);
                if (device.getBondState() == BluetoothDevice.BOND_BONDED) {
                    unpairDevice(device);
                } else {
                    showToast("Vinculando...");
                    pairDevice(device);
                }
            }
        });

        registerReceiver(mPairReceiver,
                new IntentFilter(BluetoothDevice.
                        ACTION_BOND_STATE_CHANGED));
    }

    private void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    private void pairDevice(BluetoothDevice device) {
        try {
            Method method = device.getClass().getMethod("createBond",
                    (Class[]) null);
            method.invoke(device, (Object[]) null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void unpairDevice(BluetoothDevice device) {
        try {
            Method method = device.getClass().getMethod("removeBond",
                    (Class[]) null);
            method.invoke(device, (Object[]) null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private final BroadcastReceiver mPairReceiver =
            new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    String action = intent.getAction();
                    if (BluetoothDevice.ACTION_BOND_STATE_CHANGED.
                            equals(action)) {

                        final int state =
                                intent.getIntExtra(BluetoothDevice.
                                                EXTRA_BOND_STATE,
                                        BluetoothDevice.ERROR);

                        final int prevState =
                                intent.getIntExtra(BluetoothDevice
                                                .EXTRA_PREVIOUS_BOND_STATE,
                                        BluetoothDevice.ERROR);

                        if (state == BluetoothDevice.BOND_BONDED &&
                                prevState == BluetoothDevice.BOND_BONDING) {
                            showToast("Vinculado");
                        } else if (state == BluetoothDevice.BOND_NONE &&
                                prevState == BluetoothDevice.BOND_BONDED) {
                            showToast("Desvinculado");
                        }
                        mAdapter.notifyDataSetChanged();
                    }
                }
            };

    @Override
    public void onDestroy() {
        unregisterReceiver(mPairReceiver);
        super.onDestroy();
    }
}